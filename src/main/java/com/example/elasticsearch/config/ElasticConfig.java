package com.example.elasticsearch.config;


import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import jakarta.websocket.RemoteEndpoint;
import org.apache.http.*;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.message.BasicHeader;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchClients;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;


@Configuration

public class ElasticConfig {



    @Bean
    public RestClient getRestClient() {
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
            new UsernamePasswordCredentials("elastic", "changeme")); // replace with your actual username and password

        RestClientBuilder builder = RestClient.builder(new HttpHost("localhost", 9200))
            .setHttpClientConfigCallback(httpClientBuilder ->
                httpClientBuilder
                    .setDefaultCredentialsProvider(credentialsProvider)
                    .addInterceptorFirst((HttpRequestInterceptor) (request, context) -> {
                        if (!request.containsHeader("X-Elastic-Product")) {
                            request.addHeader("X-Elastic-Product", "Elasticsearch");
                        }
                    })   .addInterceptorLast((HttpResponseInterceptor) (response, context) -> {
                        System.out.println("Response: " + response.getStatusLine().toString());
                    })

            );

        builder.setDefaultHeaders(
                new Header[]{new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json")
//                        ,
//                        new BasicHeader("X-Elastic-Product", "Elasticsearch")
                }
        );

        return builder.build();
    }

    @Bean
    public ElasticsearchTransport getElasticsearchTransport() {
        return new RestClientTransport(getRestClient(), new JacksonJsonpMapper());

    }

    @Bean
    public ElasticsearchClient getElasticSearchClient() {
        ElasticsearchClient client =
                new ElasticsearchClient(getElasticsearchTransport());
        return client;
    }

}
