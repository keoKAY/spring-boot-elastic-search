package com.example.elasticsearch.feature;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//extends ElasticsearchRepository<Product, String>{
@Repository
public class ProductRepository   {
    public static final String PRODUCTS = "products";
    @Autowired
    private ElasticsearchClient elasticsearchClient;


    public String createOrUpdate(Product product) throws IOException {
        IndexResponse response = elasticsearchClient.index(
                i -> i.index(PRODUCTS)
                        .id(product.getId())
                        .document(product));

        Map<String, String> responseMessages = Map.of(
                "created", "Product created",
                "updated", "Product updated"
        );
        return responseMessages.getOrDefault(response.result().name(), "Error has occurred");
    }


//  for find all
    public List<Product> findAll() throws IOException{
        SearchRequest request = SearchRequest.of(d-> d.index(PRODUCTS));
        SearchResponse<Product> response = elasticsearchClient.search(request, Product.class);

        List<Product> products = new ArrayList<>();
        response.hits().hits().forEach(h -> products.add(h.source()));
        return products;
    }

    public Product findDocById(String productId) throws IOException {

        System.out.println("Product ID : "+productId);
        return elasticsearchClient.get(g -> g.index(PRODUCTS)
                .id(productId),
                Product.class)
                .source();
    }


}
