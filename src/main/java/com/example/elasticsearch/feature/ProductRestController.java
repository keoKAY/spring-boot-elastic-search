package com.example.elasticsearch.feature;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/product")
public class ProductRestController {
    private final ProductRepository elasticSearchQuery;
    @PostMapping
    public ResponseEntity<String> createOrUpdateDocument(@RequestBody Product product) throws IOException {
        String response = elasticSearchQuery.createOrUpdate(product);
        return new ResponseEntity<>(response, HttpStatus.OK);


    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getById(@PathVariable String id) throws IOException {
        Product product = elasticSearchQuery.findDocById(id);
        return new ResponseEntity<>(product, HttpStatus.OK);

    }

    @GetMapping("/find-all")
    public ResponseEntity<?> findAll() throws IOException {
        return new ResponseEntity<>(elasticSearchQuery.findAll(), HttpStatus.OK);
    }


}
