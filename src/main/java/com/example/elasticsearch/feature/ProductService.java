package com.example.elasticsearch.feature;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


public interface ProductService {

    Product save(Product product);
    Product update(Product product);
    void delete(String id);
    Iterable<Product> findAll();
    Product getById(String id);




}
